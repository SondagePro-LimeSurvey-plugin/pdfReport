��          �      L      �  &   �  �   �     �     �      �     �  G   �     1  #   I     m     �     �  s   �  4   )     ^  �   q        
   >  �  I  3   �  �   -  )        0     H  /   e  >   �     �  7   �  ,   +	     X	     o	  �   �	  E   
     a
  �   n
  +   f     �            
                              	                                                          Allow public download (with the link). Allow to download pdf after submitted the survey, see plugin settings for url.Optionnaly replace the default print answer by a dowload link of the pdf. Basic admin notification Confirmation email Content and subject of the email Detailed admin notification For the name of the uploaded file. By default usage of questioncode.pdf Name of saved PDF file. Optionnal email to send pdf Report. Replace public print answer. Send it by email to Sub title for the pdf. The pdf are saved inside question answers, it's better if you hide the question, else only answers part are hidden. This don't deactivate limesurvey other email system. Title for the pdf. To allow user to get the file of the question number X at end : you can use this url: %s. Replacing %s by the question number (LimeSurvey replace %s by the survey number). Use this question as pdf report. pdf report Project-Id-Version: pdfReport
POT-Creation-Date: 2017-03-20 18:38+0100
PO-Revision-Date: 2017-03-20 18:40+0100
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.12
X-Poedit-Basepath: ../..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: _translate
X-Poedit-SearchPath-0: .
 Permettre le téléchargement public (avec le lien) Permet le téléchargement du fichier pdf après la soumission du questionnaire, voir les paramètres de l'extension pour le lien. Peut remplacer le système d'impression des réponses par le téléchargement du pdf. Notification simple à l’administrateur Courriel de confimation Contenu et sujet du courriel Notification détaillée à l’administrateur  Pour le nom du fichier émis. Par défaut codedelaquestion.pdf Nom du fichier pdf sauvegardé Adresse de courriel optionnelle pour l'envoi du rapport Remplacer le lien d'impression des réponses L'envoyer à l'adresse Sous-titre du pdf Le fichier pdf sera sauvé en tant que réponse à la question, il est préférable de masquer la question, sinon seule la partie réponse sera masquée. Ceci ne désactive pas l'envoie par les autres système de LimeSurvey Titre du pdf Pour permettre à l'utilisateur de télécharger le pdf associé à une question à la fin du questionnaire : vous pouvez utiliser ce lien : %s. En remplaçant %s par le numéro de question (LimeSurvey remplace %s par le numéro de questionnaire). Utiliser cette question pour le rapport pdf rapport pdf 